<?php

namespace App\Models;

use App\Models\User;
use App\Models\PaymentType;
use App\Models\DeliveryMethod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model {
    use HasFactory;

    protected $fillable = [
        'user_id',
        'delivery_method_id',
        'payment_type_id',
        'comment',
        'sum',
        'products',
        'address',
    ];

    public $casts=[
        "products" => "array",

        "address" => "array",

    ];
    

    public function user() {
        return $this->belongsTo(User::class);

    }

    public function paymentType() {
        return $this->belongsTo(PaymentType::class);        
    }

    public function deliveryMethod() {
        return $this->belongsTo(DeliveryMethod::class);
    
    }
}
