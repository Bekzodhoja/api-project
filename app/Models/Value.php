<?php

namespace App\Models;

use App\Models\Product;
use App\Models\Attribute;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Value extends Model
{
    use HasFactory , HasTranslations;

    public $translatable =["name"];

    protected $fillable = [
        "name",
        "product_id",
        "attribute_id",
    ];

    public function attributes(){
        return $this->belongsTo(Attribute::class);
    }
 
    public function products(){
        return $this->belongsTo(Product::class);

    }
}
