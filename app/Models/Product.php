<?php

namespace App\Models;

use App\Models\User;
use App\Models\Stock;
use App\Models\Value;
use App\Models\Category;
use App\Models\Attribute;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory , HasTranslations, SoftDeletes;  
    protected $fillable = [
        "name",
        "description",
        "quantity",
        "category_id",
    ];

    public $translatable =["name","description"];
    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function stocks(){
        return  $this->hasMany(Stock::class);
    }

    public function values(){
        return $this->hasMany(Value::class);
    }


    public function users()
    {
        return $this->belongsToMany(User::class);

    }

    public function withStock($stockId)
    {
              $this->stocks=[$this->stocks()->findOrFail($stockId)];
              return $this;
    }
}
