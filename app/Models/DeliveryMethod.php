<?php

namespace App\Models;

use App\Models\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Translatable\HasTranslations;

class DeliveryMethod extends Model
{
    use HasFactory ,HasTranslations;

    protected $fillable = [
        'name',
        'estimated_time',
        'sum',

    ];

    public $translatable=['name','estimated_time'];

    public function deliveryMethod()
    {
        return $this->hasMany(Order::class);
    }

    
}
