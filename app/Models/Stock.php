<?php

namespace App\Models;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Stock extends Model
{
    use HasFactory;
    protected $fillable = [
        "product_id",
        "attributes",
        "quantity",
    ];

    public function products(){
        return $this->belongsTo(Product::class);
    }
}
