<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\UserAddress;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ProductResource;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Stock;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth:sanctum");
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return  auth()->user()->orders;
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreOrderRequest $request)
    {
        $sum = 0;
        $products = [];
        $notFoundProducts=[];
        $address = UserAddress::find($request->address_id);


        foreach ($request['products'] as $requestProduct) {
            $product = Product::with("stocks")->findOrFail($requestProduct['product_id']);
            $product->quantity = $requestProduct['quantity'];
            if (
                $product->stocks->find($requestProduct['stock_id']) &&
                $product->stocks->find($requestProduct['stock_id'])->quantity >= $requestProduct['quantity']
            ) {
                $productWithStock = $product->withStock($requestProduct['stock_id']);
                $productResource = new ProductResource($productWithStock);

                $sum += $productResource['price'];

                $products[] = $productResource->resolve();
            }else{
                // $requestProduct['quantity'] = $product->stocks->find($requestProduct['stock_id'])->quantity;
                $notFoundProducts[] = $requestProduct;
            }
        }


if($notFoundProducts == [] && $products != [] && $sum != 0){
        $order=auth()->user()->orders()->create([
            "delivery_method_id" => $request->delivery_method_id,
            "payment_type_id" => $request->payment_type_id,
            "comment" => $request->comment,
            "sum" => $sum,
            "products" => $products,
            "address" => $address
        ]);

        if($order){
            foreach($products as $product)
            {
                $stock = Stock::find($product['stocks'][0]['id']);
                $stock->quantity -= $product['order_quantity'];
                $stock->save();
            }
        }

        return 'Success';
    }else{
        return response([
            'Success' => false,
            "message"=>"Product not found sory",
            'Products' => $notFoundProducts,

        ]);
    }
    }
    /**
     * Display the specified resource.
     */
    public function show(Order $order)
    {
        return new  OrderResource($order);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateOrderRequest $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Order $order)
    {
        //
    }
}
