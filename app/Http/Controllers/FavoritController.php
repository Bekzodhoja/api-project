<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FavoritController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth:sanctum");
    }
    public function index()
    {
        return auth()->user()->favorits()->paginate(3);
    }

    public function store(Request $request)
    {

        auth()->user()->favorites()->attach($request->product_id);
        return response()->json(['success' => true]);
    }


    public function destroy($favorit_id)
    {

        if (auth()->user()->hasFavorit($favorit_id)) {
            auth()->user()->favorites()->detach($favorit_id);
            return response()->json(['success' => true]);
        }

        return response()->json(['success' => false]);
    }
}
