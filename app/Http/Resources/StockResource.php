<?php

namespace App\Http\Resources;

use App\Models\Value;
use App\Models\Attribute;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;



class StockResource extends JsonResource
{

    public function toArray(Request $request): array
    {
        $result= [
            'stock_id'=>$this->id,
            'quantity'=> $this->quantity,
          ];
    return $this->getAttributes($result);

    

}
public function getAttributes(array $result): array
{
    $attributes = json_decode($this->attributes, true);
    foreach ($attributes as $stockAttribute) {
        $attributeId = $stockAttribute['attribute_id'];
        $valueId = $stockAttribute['value_id'];

        $attribute = Attribute::find($attributeId);
        $value = Value::find($valueId);

        $result[$attribute->name] = $value->getTranslations('name');
    }

    return $result;
}


}