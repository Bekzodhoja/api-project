<?php

namespace App\Http\Resources;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Resources\StockResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        // dd($this->category->name);
        return [
            "id" => $this->id,
            "name" => $this->getTranslations('name'),
            "price" => $this->price,
            "description" => $this->getTranslations('description'),
            "category" => new CategoryResource($this->category),
            "stocks" => StockResource::collection($this->stocks),
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at,
            "order_quantity" => $this->when(isset($this->quantity), $this->quantity),
        ];
    }
}
