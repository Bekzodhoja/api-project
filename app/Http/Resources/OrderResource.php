<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return 
        [
            "id" => $this->id,
            "delivery_method_id" => $this->delivery_method_id, 
            "payment_type_id" => $this->payment_type_id,
            "comment" => $this->comment,
            "sum" => $this->sum,
            "products" => $this->products,
            "address" => $this->address
            ];
        
    }
}
