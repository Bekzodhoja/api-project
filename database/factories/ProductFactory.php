<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "category_id"=>rand(1,3),
           "name"=>[
            'en'=>fake()->sentence(4),
            'uz'=>fake()->sentence(4),
           ],
           "description"=>[
            "en"=>fake()->paragraph(5),
            "uz"=>fake()->paragraph(5),
           ],
           "price"=>rand(10000, 1000000),
          
        ];
    }
}
