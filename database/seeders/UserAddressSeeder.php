<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::find(1)->addresses()->create([
            
            "latitude"=>"40.7143528",
            "langitude"=>"-74.0059731",
            "region"=>"New York",   
            "district"=>"Manhattan",
            "street"=>"Manhattan",
            "home"=>"1"

        ]);
        User::find(2)->addresses()->create([
            
            "latitude"=>"61.7143528",
            "langitude"=>"85.0059731",
            "region"=>"Tashkent",   
            "district"=>"Chirchik",
            "street"=>"Marifat",
            "home"=>"31"

        ]);
    }
}
