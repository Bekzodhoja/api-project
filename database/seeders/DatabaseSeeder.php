<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Stock;
use Illuminate\Database\Seeder;
use Database\Seeders\RoleSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\StockSeeder;
use Database\Seeders\ValueSeeder;
use Database\Seeders\ProductSeeder;
use Database\Seeders\CategorySeeder;
use Database\Seeders\AttributeSeeder;
use Database\Seeders\UserAddressSeeder;
use Database\Seeders\DeliveryMethodSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
       $this->call([
        CategorySeeder::class,
        RoleSeeder::class,
        UserSeeder::class, 
        ProductSeeder::class,

        AttributeSeeder::class, 
        ValueSeeder::class,
        DeliveryMethodSeeder::class,
        PaymentTypeSeeder::class,
        UserAddressSeeder::class,
        
       ]);
    }
}
