<?php

namespace Database\Seeders;

use App\Models\Value;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Value::create([
            "attribute_id"=>1,
            "name"=> [
                "en"=> "Red",
                "uz"=> "Qizil",
            ],
        ]);
        Value::create([
            "attribute_id"=>1,
            "name"=> [
                "en"=> "Black",
                "uz"=> "Qora",
            ],
        ]);
        Value::create([
            "attribute_id"=>1,
            "name"=> [
                "en"=> "Blue",
                "uz"=> "Ko'k",
            ],
        ]);


        Value::create([
            "attribute_id"=>2,
            "name"=> [
                "en"=> "Smol",
                "uz"=> "Kichkina",
            ],
        ]);
        Value::create([
            "attribute_id"=>2,
            "name"=> [
                "en"=> "Norm",
                "uz"=> "O'rta",
            ],
        ]);
        Value::create([
            "attribute_id"=>2,
            "name"=> [
                "en"=> "Big",
                "uz"=> "Katta",
            ],
        ]);

        Value::create([
            "attribute_id"=>3,
            "name"=> [
                "en"=> "MDF",
                "uz"=> "MDF",
            ],
        ]);
        Value::create([
            "attribute_id"=>3,
            "name"=> [
                "en"=> "LDSP",
                "uz"=> "LDSP",
            ],
        ]);
        Value::create([
            "attribute_id"=>3,
            "name"=> [
                "en"=> "Metal",
                "uz"=> "Metal",
            ],
        ]);
    }
}
