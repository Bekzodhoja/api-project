<?php

namespace Database\Seeders;

use App\Models\DeliveryMethod;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DeliveryMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DeliveryMethod::create([
            "name"=>[
                "en"=>"Free delivery",
                "uz"=>"Bepul yetkazish"
            ],

            "estimated_time"=>[
                "en"=>"5 days",
                "uz"=>"5 kun"
            ],
            "sum"=>0,

        ]);

        DeliveryMethod::create([
            "name"=>[
                "en"=>"Standart delivery",
                "uz"=>"Standart yetkazish"
            ],

            "estimated_time"=>[
                "en"=>"3 days",
                "uz"=>"3 kun"
            ],
            "sum"=>50000,

        ]);
        DeliveryMethod::create([
            "name"=>[
                "en"=>"Fast delivery",
                "uz"=>"Tez yetkazish"
            ],

            "estimated_time"=>[
                "en"=>"1 days",
                "uz"=>"1 kun"
            ],
            "sum"=>10000,

        ]);
    }
}
