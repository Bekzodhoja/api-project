<?php

namespace Database\Seeders;

use App\Models\PaymentType;
use Faker\Provider\ar_EG\Payment;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PaymentType::create([
            "name"=>[
                "en"=>"Cash",
                "uz"=>"Naqd"
            ]
            ]);

            PaymentType::create([
                "name"=>[
                    "en"=>"Payme",
                    "uz"=>"Payme"
                ]
                ]);
                PaymentType::create([
                    "name"=>[
                        "en"=>"Click",
                        "uz"=>"Click"
                    ]
                    ]);
    }
}
