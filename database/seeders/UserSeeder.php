<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $admin = User::create([
            "first_name" => "Admin",
            "last_name" => "admin",
            "email" => "a@gmail.com",
            "phone" => "+998999999999",
            "password" => "admin",
        ]);

        $user = User::create([
            "first_name" => "User",
            "last_name" => "user",
            "email" => "u@gmail.com",
            "phone" => "+998888888888",
            "password" => "password",
        ]);


        $admin->roles()->attach(2);
        $user->roles()->attach(1);
        User::factory()->count(10)->hasAttached([Role::find(2)])->create();
    }
}
